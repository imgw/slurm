#!/usr/bin/env python3

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def fibi(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a

memo = {0:0, 1:1}
def fibm(n):
    if not n in memo:
        memo[n] = fibm(n-1) + fibm(n-2)
    return memo[n]

if __name__ == "__main__":
    import time
    a = time.time()
    print("Recursive version")
    print(fib(41), end=', ')
    print('Elapsed:', time.strftime("%Hh%Mm%Ss", time.gmtime(time.time()-a)))

    a = time.time()
    print("Loop version")
    print(fibi(41), end=', ')
    print('Elapsed:', time.strftime("%Hh%Mm%Ss", time.gmtime(time.time()-a)))
    
    a = time.time()
    print("Memory version")
    print(fibm(41), end=', ')
    print('Elapsed:', time.strftime("%Hh%Mm%Ss", time.gmtime(time.time()-a)))
