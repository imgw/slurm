# Introduction to SLURM on JET

Slurm is an open source, fault-tolerant, and highly scalable cluster management and job scheduling system for large and small Linux clusters.

[[_TOC_]]

# Split your work into JOBs

In order to make use of a super computer with multiple users a queuing system is required and workloads need to be split into jobs. [SLURM](https://slurm.schedmd.com/documentation.html) is a workload manager for HPCs and used on VSC and JET. Here is a quickstart guide [here](https://slurm.schedmd.com/quickstart.html) and in the sections below some information on how to solve the most common challenages is shown.

[Short Summary of Commands](docs/Slurm-cheatsheet.pdf)


## First Job
```bash
>>>squeue 
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON) 
```
What does that mean? The Queue is empty and is waiting for your jobs. 
So let's give the computer something to think about.
Open your favorite Editor (Gedit, Emacs, vim, nano, ...) and put the code below into the file, save it as `test.job`
```bash
#!/bin/bash
# SLURM specific commands
#SBATCH --job-name=test
#SBATCH --output=test.log
#SBATCH --ntasks=1
#SBATCH --time=01:30

# Your Code below here
srun hostname
srun sleep 60
```
Now we submit this job to the queue:
```bash
sbatch test.job
```
and look agin into the queue

```
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON) 
               211   compute test_mpi mblasche  R       0:02      1 jet03 
```
It is running on jet03 and about a minute later it should be finished and you should see the log file `test.log` with something like that in it:
```
jet03.jet.local
```
The hostname of the node running our test script. 

## Leasons learnt
Of course that is a silly job. However, there are things to learn for this example:
1. Writing a `job` file and using the `#SBATCH` commands to specify
    * `job-name`, this is just for you
    * `output`, this is just for you
    * `ntasks`, this tells slurm how many threads you should be allowed to have
    * `time`, this tells slurm how much time it should give your job
    * other `#SBATCH` commands [https://slurm.schedmd.com/sbatch.html](https://slurm.schedmd.com/sbatch.html)
2. Submit the job with `sbatch`
3. Check the Queue and Status of your job with `squeue`

# Writing a job file

There are things to consider when writing a job:
1. How many `tasks` or threads (each CPU has 2 threads) the job should be allowed to use.
2. How much `time` your Jobs will need. This can be quite tricky. If you make your job too short, the program will be killed. If it is too long the job will take longer to start. Although that is currently no problem.

Please find the corresponding file in [example](example/) folder.


```bash
#!/bin/bash
# SLURM specific commands
#SBATCH --job-name=fibonacci
#SBATCH --output=fibonacci.log
#SBATCH --ntasks=1
#SBATCH --time=05:00

# Your Code below here
module load miniconda3
# Execute the Fibonacci Script with the miniconda Python
# use /usr/bin/time -v [program]
# gives statistics on the resources the program uses
# nice for testing
/usr/bin/time -v python3 fibonacci.py
```
Save the job as `fibonacci.job` and submit the job with `sbatch fibonacci.job`. 
Let's have a look at the output.
```
Recursive version
165580141, Elapsed: 00h00m55s
Loop version
165580141, Elapsed: 00h00m00s
Memory version
165580141, Elapsed: 00h00m00s
	Command being timed: "python3 fibonacci.py"
	User time (seconds): 55.84
	System time (seconds): 0.00
	Percent of CPU this job got: 99%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:55.94
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 10900
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 1288
	Voluntary context switches: 9
	Involuntary context switches: 22
	Swaps: 0
	File system inputs: 0
	File system outputs: 0
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
```
You can find information on the CPU usage, memory, ... For this job the only relevant information is on the CPU, as it basically only uses that.
The time man page is [here](https://man7.org/linux/man-pages/man1/time.1.html).

## Run a Multi-processing job

**under construction.**
`icc -o fibonacci.x fibonacci_openmp.c`
ask for more tasks

# Interactive Sessions

It is possible to ask for an interactive session on any node. When your normal job runs you can always `ssh` to the allocated nodes. However, the file system is global, so checking files can be done from the login nodes as well.


## How to run an interactive job

An interactive session can be started like this

```bash
# This will give you a full node
$ srun --pty bash
srun: job 18351 queued and waiting for resources
srun: job 18351 has been allocated resources
[user@n3502-045 ~]$ 
```

or using an allocation:

```bash
# Create an allocation for 2 hours (1Node)
$ salloc --time=02:00:00
salloc: Pending job allocation 18356
salloc: job 18356 queued and waiting for resources
salloc: job 18356 has been allocated resources
salloc: Granted job allocation 18356
salloc: Waiting for resource configuration
salloc: Nodes n3501-009 are ready for job
# Connect to that allocation by running
$srun --pty bash 
[user@n3501-009 ~]$
# or use ssh
$ssh n3501-009
[user@n3501-009 ~]$
```

*Please note the following:*

Using `srun` will create a terminal session (pty) on the compute node and allow you to run programs interactively. Once you disconnect with `exit` or `CTRL+c` from the job, all running processes from that terminal session will be killed. It is not possible to put things into the background. When you connect with `ssh` it is possible to put processes into the background and exit from the ssh session.

**Disconnecting from the login node will kill your salloc**

### interactive long jobs

If you need to have a development job, where you would like to run things and **connect and disconnect repeatedly** the above setup will fail. Use a terminal multiplexer for that. Follow these steps:

```bash
# connect to a login node on VSC or JET or ...
# launch a terminal multiplexer: tmux, screen, ...
$ tmux 
# now you are inside the Tmux session
# allocate for 3 days
$ salloc  --time=3-00:00:00
salloc: Pending job allocation 18356
salloc: job 18356 queued and waiting for resources
salloc: job 18356 has been allocated resources
salloc: Granted job allocation 18356
salloc: Waiting for resource configuration
salloc: Nodes n3501-009 are ready for job
# connect to the Node
$ srun --pty bash
[user@n3501-009 ~]$
# or using ssh
$ ssh n3501-009
[user@n3501-009 ~]$
```

When you disconnect from the compute Node 

TMux control shortcuts
![](docs/tmux.png)

more information on [Terminal Multiplexer](https://opensource.com/article/21/5/linux-terminal-multiplexer).

## How to run on a specific node

The following options also work with non-interactive jobs (sbatch, salloc, etc.)

```bash
# use the -w / --nodelist option
srun -w jet05 --pty bash -i
```
How to acquire each node exclusively (only relevant for shared Nodes, e.g. Jet)

```bash
# use the --exclusive option
srun -w jet05 --exclusive --pty bash -i
```

# MPI

How to submit an MPI batch job

```bash
#!/bin/bash
#SBATCH --job-name=hello
#SBATCH --output=hello.log
#SBATCH --ntasks=7

# Load the OpenMPI module.
module load openmpi/4.0.5

# Run the program with mpi. Srun will execute mpirun with
# the info (e.g., hosts) from SLURM.
srun ./hello
```

Submit with
```bash
sbatch hello.job
```

# Useful things

## Information on the Job
```bash
# show all completed jobs
sacct -a -s cd

# show all jobs from user
sacct -a -u $USER

# show info for that job
sacct -j <JOBID>

# show more info for that job
sacct -l -j <JOBID>

# Memory Used
sacct -j <JOBID> --format=JobID,JobName,ReqMem,MaxRSS,Elapsed
```

## Send an Email Notification of your job:

possible mail-types: `ALL, BEGIN, END, FAIL`

```bash
#SBATCH --mail-type=BEGIN    # first have to state the type of event to occur 
#SBATCH --mail-user=<email@address.at>   # and then your email address
```

## seff

TODO 🚧 **being edited** 🚧